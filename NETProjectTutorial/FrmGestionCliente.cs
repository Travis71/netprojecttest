﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionCliente : Form
    {
        private DataSet dsSistema;
        private BindingSource bsCliente;

        public FrmGestionCliente()
        {
            InitializeComponent();
            bsCliente = new BindingSource();
        }

        public DataSet DsSistema
        {
            get { return dsSistema; }
            set { dsSistema = value; }
        }

        private void FrmGestionCliente_Load(object sender, EventArgs e)
        {
            bsCliente.DataSource = dsSistema;
            bsCliente.DataMember = "Cliente";
            dgvCliente.DataSource = bsCliente;
        }
    }
}
