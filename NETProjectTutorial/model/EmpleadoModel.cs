﻿using Nancy.Json;
using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {
        private static List<Empleado> Lstempleados = new List<Empleado>();

        public static List<Empleado> GetLstEmpleado()
        {
            return Lstempleados;
        }

        public static void Populate()
        {
            Empleado[] empleados =
            {
                new Empleado(1,"1658574-2","001-110288-5588D",
                "Pepito","Perez","del arbolito 2C. abajo","22558877",
                "88774455",Empleado.SEXO.MASCULINO,25145.33),
                new Empleado(2,"1655555-2","001-000000-0000D",
                "Ana","Conda","de la racachaca 1C al sur","22662182",
                "89717350",Empleado.SEXO.FEMENINO,30145.01),
                new Empleado(3,"1888123-2","001-111111-1111D",
                "Armando","Guerra","de las delicias del volga 1/2C al sur",
                "22665588","77554411",Empleado.SEXO.MASCULINO,12145.25)
            };

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Lstempleados = serializer.Deserialize<List<Empleado>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.EMPLOYEE_DATA));
          
        }
    }
}
