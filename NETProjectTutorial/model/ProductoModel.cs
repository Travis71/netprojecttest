﻿using Nancy.Json;
using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ProductoModel
    {
        private static  List<Producto> productos = new List<Producto>();


        public static List<Producto> GetProductos()
        {
            return productos;
        }

        public static void Populate()
        {
            Producto[] pdts =
            {
                new Producto(1,"Milk2514","Leche entera","Leche enterea 3% grasa",25,30.0),
                new Producto(2,"Milk2515","Leche descremada","Leche descremada 0% grasa",25,40.0),
                new Producto(3,"Milk2516","Leche deslactosada","Leche deslactosada 2% grasa",25,35.0)
            };

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            productos = serializer.Deserialize<List<Producto>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Product_data));

        }
    }
}
