﻿using NETProjectTutorial.entities;
using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmFactura : Form
    {
        private DataSet dsSistema;
        private BindingSource bsDetalle;
        private bool _canUpdate = true;
        private bool _needUpdate = false;
        private double subtotal;
        private double iva;
        private double total;

        public FrmFactura()
        {
            InitializeComponent();
            bsDetalle = new BindingSource();
        }

        public DataSet DsEmpleados { set => dsSistema = value; }
        private void FrmFactura_Load(object sender, EventArgs e)
        {
            cmbEmpleados.DataSource = dsSistema.Tables["Empleado"];
            cmbEmpleados.DisplayMember = "NA";
            cmbEmpleados.ValueMember = "Id";

            cmbMonedas.DataSource = Enum.GetValues(typeof(Monedas));

            cmbProductos.DataSource = dsSistema.Tables["Producto"];
            cmbProductos.DisplayMember = "Info";
            cmbProductos.ValueMember = "Id";

            dsSistema.Tables["ProductoFactura"].Rows.Clear();
        }


        private void UpdateData()
        {
            if (cmbEmpleados.Text.Length > 1)
            {

                List<Empleado> searchData = dsSistema.Tables["Empleado"].AsEnumerable().Select(
                    dataRow =>
                    new Empleado
                    {
                        Id = dataRow.Field<Int32>("Id"),
                        Nombres = dataRow.Field<String>("Nombres"),
                        Apellidos = dataRow.Field<String>("Apellidos")
                    }).ToList();

                HandleTextChanged(searchData.FindAll(e => e.Nombres.Contains(cmbEmpleados.Text)));
            }
            else
            {
                RestartTimer();
            }
        }


        //Actualizar el combo con nuevos datos
        private void HandleTextChanged(List<Empleado> dataSource)
        {
            var text = cmbEmpleados.Text;

            if (dataSource.Count() > 0)
            {
                cmbEmpleados.DataSource = dataSource;

                var sText = cmbEmpleados.Items[0].ToString();
                cmbEmpleados.SelectionStart = text.Length;
                cmbEmpleados.SelectionLength = sText.Length - text.Length;
                cmbEmpleados.DroppedDown = true;
                return;
            }
            else
            {
                cmbEmpleados.DroppedDown = false;
                cmbEmpleados.SelectionStart = text.Length;
            }
        }

        private void RestartTimer()
        {
            timer1.Stop();
            _canUpdate = false;
            timer1.Start();
        }

        //Update data when timer stops
        private void timer1_Tick(object sender, EventArgs e)
        {
            _canUpdate = true;
            timer1.Stop();
            UpdateData();
        }


        private void cmbEmpleados_TextChanged(object sender, EventArgs e)
        {
            if (_needUpdate)
            {
                if (_canUpdate)
                {
                    _canUpdate = false;
                    UpdateData();
                }
                else
                {
                    RestartTimer();
                }
            }
        }

        private void CmbEmpleados_TextUpdate(object sender, EventArgs e)
        {
            _needUpdate = true;
        }

        private void CmbEmpleados_SelectedIndexChanged(object sender, EventArgs e)
        {
            _needUpdate = false;
        }

        private void Timer1_Tick_1(object sender, EventArgs e)
        {
            _canUpdate = true;
            timer1.Stop();
            UpdateData();
        }

        private void cmbProductos_TextUpdate(object sender, EventArgs e)
        {
            MessageBox.Show(cmbProductos.SelectedValue.ToString());
        }


        private void cmbProductos_SelectedIndexChanged_1(object sender, EventArgs e)
        {

            txtCantidad.Text = ((DataRowView)cmbProductos.SelectedItem).Row["Cantidad"].ToString();
            txtPrecio.Text = ((DataRowView)cmbProductos.SelectedItem).Row["Precio"].ToString();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

            try
            {
                DataRow drProductoFactura = dsSistema.Tables["ProductoFactura"].NewRow();
                drProductoFactura["Sku"] = ((DataRowView)cmbProductos.SelectedItem).Row["SKU"].ToString();
                drProductoFactura["Nombre"] = ((DataRowView)cmbProductos.SelectedItem).Row["Nombre"].ToString();
                drProductoFactura["Cantidad"] = 0;
                drProductoFactura["Precio"] = Double.Parse(((DataRowView)cmbProductos.SelectedItem).Row["Precio"].ToString());
                drProductoFactura["Id"] = Int32.Parse(((DataRowView)cmbProductos.SelectedItem).Row["Id"].ToString());

                dsSistema.Tables["ProductoFactura"].Rows.Add(drProductoFactura);
                bsDetalle.DataSource = dsSistema;
                bsDetalle.DataMember = "ProductoFactura";

                dgvDetalle.DataSource = bsDetalle;
            }
            catch (ConstraintException)
            {
                MessageBox.Show(this, "Duplicado, producto ya se encuentra agregado", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
       

        private void calcularTotal()
        {
            subtotal = 0; iva = 0; total = 0;
            int i = 0;
            foreach (DataGridViewRow row in dgvDetalle.Rows)
            {
                subtotal += double.Parse(row.Cells[3].Value.ToString()) * double.Parse(row.Cells[2].Value.ToString());
                dsSistema.Tables["ProductoFactura"].Rows[i++]["Cantidad"] = double.Parse(row.Cells[2].Value.ToString());
            }

            iva = subtotal * 0.15;
            total = subtotal + iva;

            txtSubtotal.Text = subtotal.ToString();
            txtIva.Text = iva.ToString();
            txtTotal.Text = total.ToString();

        }


        private void dgvDetalle_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
        {
            int cantidadProductoCell =  Int32.Parse(dgvDetalle.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
            int indexProductoCell = Int32.Parse(dsSistema.Tables["ProductoFactura"].Rows[e.RowIndex]["Id"].ToString());
            DataRow drProductoStored = dsSistema.Tables["Producto"].Rows.Find(indexProductoCell);
            int cantidadProductoStored = Int32.Parse(drProductoStored["Cantidad"].ToString());
            //Validando que la cantidad en el almacen sea mayor o igual que la que se quiere vender
            if (cantidadProductoCell > cantidadProductoStored)
            {
                dgvDetalle.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
                Util.MessageBoxUtil.ErrorMessage(this, "Error, la cantidad a vender no debe superar al del almacen");
                return;
            }
            //si las cantidades estan correctas se calcula el subtotal, iva y total
            calcularTotal();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            int rowCount = dgvDetalle.Rows.Count;
            //validando si se agregaron productos al datagridview
            if(rowCount <= 0)
            {
                Util.MessageBoxUtil.ErrorMessage(this, "Error, debe agregar al menos un producto a la factura!");
                return;
            }

            //validando si alguna de los productos agregados al datagridview tiene en la columna Cantidad cero
            Boolean flag = false;
            foreach (DataGridViewRow row in dgvDetalle.Rows)
            {
                int cantidad = Int32.Parse(row.Cells["Cantidad"].Value.ToString());
                if (cantidad <= 0)
                {
                    flag = true;
                    break;
                }
                
            }

            if (flag)
            {
                Util.MessageBoxUtil.ErrorMessage(this, "Error, cantidad de productos no puede ser cero!");
                return;
            }

            DataRow drFactura = dsSistema.Tables["Factura"].NewRow();
            drFactura["Id"] = dsSistema.Tables["Factura"].Rows.Count + 1;
            drFactura["Cod_Factura"] = "F" + dsSistema.Tables["Factura"].Rows.Count + 1;
            drFactura["Fecha"] = DateTime.Now;
            drFactura["Subtotal"] = subtotal;
            drFactura["Iva"] = iva;
            drFactura["Total"] = total;
            drFactura["Empleado"] = Int32.Parse(cmbEmpleados.SelectedValue.ToString());
            drFactura["Observaciones"] = txtObservaciones.Text;
            drFactura["Moneda"] = cmbMonedas.SelectedItem.ToString();
            drFactura["Estado"] = Estado_Factura.APROBADA;

            dsSistema.Tables["Factura"].Rows.Add(drFactura);


            foreach (DataRow drPF in dsSistema.Tables["ProductoFactura"].Rows)
            {
                DataRow drDetalleFactura = dsSistema.Tables["DetalleFactura"].NewRow();
                drDetalleFactura["Id"] = dsSistema.Tables["DetalleFactura"].Rows.Count + 1;
                drDetalleFactura["Factura"] = drFactura["Id"];
                drDetalleFactura["Producto"] = drPF["Id"];
                drDetalleFactura["Cantidad"] = drPF["Cantidad"];
                drDetalleFactura["Precio"] = drPF["Precio"];

                dsSistema.Tables["DetalleFactura"].Rows.Add(drDetalleFactura);
            }


            FacturaReport fr = new FacturaReport();
            fr.DsSistema = dsSistema;
            fr.Show();

            Dispose();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rows = dgvDetalle.SelectedRows;
            if(rows.Count <= 0)
            {
                Util.MessageBoxUtil.ErrorMessage(this,"Error, no hay filas para eliminar!");
                return;
            }
            DataGridViewRow dgvrow = rows[0];

            DataRow drProductoToDelete = ((DataRowView)dgvrow.DataBoundItem).Row;

            dsSistema.Tables["ProductoFactura"].Rows.Remove(drProductoToDelete);
        }
    }
}
