﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionFacturas : Form
    {
        private DataSet dsSistema;
        private BindingSource bsFactura;

        public FrmGestionFacturas()
        {
            InitializeComponent();
            bsFactura = new BindingSource();
        }

        public DataSet DsSistema { get => dsSistema; set => dsSistema = value; }

        private void FrmGestionFacturas_Load(object sender, EventArgs e)
        {
            bsFactura.DataSource = dsSistema;
            bsFactura.DataMember = "Factura";

            dgvFactura.DataSource = bsFactura;
        }

        private void btnWatch_Click(object sender, EventArgs e)
        {
            FacturaReport fr = new FacturaReport();
            fr.MdiParent = this.MdiParent;
            fr.DsSistema = dsSistema;
            fr.Show();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {

        }
    }
}
